SELECT planes.name
FROM (
  SELECT name, eid, modelId
  FROM (
    SELECT name, eid, planeId
    FROM Inspector, Employee, InspectedBy
    WHERE iid = inspectionId
    AND   eid = employeeId
  ) AS inspections
  LEFT JOIN Plane ON (pid = planeId)
) AS planes
LEFT JOIN Model ON (mid = modelId)
GROUP BY eid
HAVING COUNT(DISTINCT manufacturer) = 1;
