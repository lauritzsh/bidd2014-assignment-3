SELECT COUNT(DISTINCT employeeId)
FROM Inspector, InspectedBy
WHERE planeId IN (
  SELECT pid
  FROM Plane
  WHERE airline = "SAS"
)
AND inspectionId = iid;
