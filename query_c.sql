DELETE i, ib
FROM Inspector i
JOIN InspectedBy ib ON i.iid = ib.inspectionId
WHERE i.date < "2000-01-01";
